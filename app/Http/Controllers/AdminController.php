<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

class AdminController extends Controller
{
     public function index()
    {

        //$posts = Post::latest()->paginate(5);
        $posts = Post::IsPostApproved(0)->latest();


        return view('admin.posts',compact('posts'));
    }


    public function edit($id) {
    	$posts= Post::find($id);
        return view('admin.edit',compact('posts'));
    }

    public function update(Request $request, $id)
    {
        Post::find($id)->update($request->all());
        return redirect('admin/posts');
    }
     public function destroy($id)
    {
       Post::destroy($id);
        return redirect('admin/posts');
                        //->with('success','Product updated successfully');
    }
}
