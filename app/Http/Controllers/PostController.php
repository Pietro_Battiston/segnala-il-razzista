<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;


use App\Post;

class PostController extends Controller
{

    private $imgPath = "/img/upload/";
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$posts = Post::latest()->paginate(5);
        $posts = Post::IsPostApproved(0)->latest()->paginate(1);


        return view('welcome',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('create_post', ['post' => new Post()]);
    }

    private function UploadImage(Request $request)
    {
            if ($request) {
                    $destinationPath = './img/';
                    $image = $request->file('img');
                    $fileName = $image->getClientOriginalName();
                    $randomNumber = rand(11111,99999);
                    $newFileName = $randomNumber . "_" . $fileName;
                    //$image->move($destinationPath, $newFileName); 
                    // $tocompress = Tinify::fromFile($image);
                    // $tocompress->toFile($newFileName);
                    $movefile = rename($newFileName, $destinationPath . $newFileName);
                return $newFileName;
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        // With the following we call the Validator. It is in Http/Request/StorePostRequest. The Controller is cleaner this way.
        $post = $request->validated();

        if($request->screen_src) {
                    $destinationPath = './img/';
                    $image = $post["screen_src"];
                    $fileName = $image->getClientOriginalName();
                    $randomNumber = rand(11111,99999);
                    $newFileName = $randomNumber . "_" . $fileName;
                    $image->move($destinationPath, $newFileName); 
                    // $tocompress = Tinify::fromFile($image);
                    // $tocompress->toFile($newFileName);
                    //$movefile = rename($newFileName, $destinationPath . $newFileName);
                     //$request->screen_src = $newFileName;
                     $post["screen_src"] = $newFileName;
                     
               
        }
       
        $readyToStore = $post;
        return Post::create($readyToStore);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
