<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|max:150',
            'profile_link' => 'nullable|max:255',
            'comment' => 'nullable',
            'screen_src' => 'mimes:jpeg,jpg,png,gif|nullable|max:10000',
            'avatar_link' => 'nullable|max:255',
            'reporter' => 'nullable|max:150',
            'social' => 'nullable|max:1|integer'
   
        ];
    }
}
