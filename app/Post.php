<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	protected $casts = [
        'approved' => 'boolean',
    ];

    protected $fillable = ['id', 'profile_link', 'name', 'comment', 'screen_src'];




//prefixing the method with "scope" allow us to reuse our query logic. In controller we must call it without "scope", only IsNotApproved. We can make it dynamic, passing to it a paramemeter ($is_approved) In controller we can now use $posts = Post::IsPostApproved(0)->latest()->paginate(5);. 0 equivale a $is_approved
    public function scopeIsPostApproved($query, $is_approved) {
    	return $query->where('approved', $is_approved);
    }

}
