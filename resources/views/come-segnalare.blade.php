@extends('layouts.app')

@section('content')
        <div class="flex-center col-lg-12 half-height-1 half-top" id="come-segnalare-header">
            <h1>COME SEGNALARCI UN POST RAZZISTA</h1>

        </div>
        <div class="col-lg-12 half-height-2 half-bottom" id="come-segnalare-content">
            <h2>Su quale Social Network vuoi diventare un Cacciatore di razzisti?</h2>
            <div class="button-scopri" id="segnala-quale-social">
                
                    <button>
                        <a href="/come-segnalare#facebook" title="facebook" id="comesegnalafacebook">
                            FACEBOOK
                        </a>
                    </button>
                    <button>
                        <a href="/come-segnalare#facebook" title="twitter" id="comesegnalatwitter">
                            TWITTER
                        </a>
                    </button>
                
            </div>
            <div class="links">
                    <a href="/come-segnalare" title="come segnalare">Come segnalare</a>
                    <a href="/perche-lo-facciamo" title="perchè lo facciamo">Perchè lo facciamo</a>
                    <a href="/donazioni" title="donazioni">Donazioni</a>
            </div>

        </div>

        <div class="container hide" id="come-segnalare-facebook">
            <div class="col-md-12 style">
                <h2>Come segnalarci contenuti razzisti su Facebook</h2>
            </div>
                <div class="col-lg-12 style">
                    <h3>DA PC</h3>     
                </div>
                <div class="col-lg-12 style">
                    <p>Sia per un post che per un commento, ti basta cliccare sull'orario (o data) di pubblicazione come vedi nelle immagini sottostanti. Copia il link della pagina che ti si aprirà e inviacelo cliccando su <a href="/segnala-razzista" title="segnala-link">questo link</a>.</p>
                </div>
                <div class="col-lg-12 style">
                    <img class="img-responsive" src="{{ asset('img/site/come_segnalare_fb_post.png') }}"></img>
                </div>
                <div class="col-lg-12 style">
                    <img class="img-responsive" src="{{ asset('img/site/come_segnalare_fb_comment.png') }}"></img>
                </div>
                <div class="col-lg-12 style">
                    <img class="img-responsive" src="{{ asset('img/site/come_segnalare_fb_link.png') }}"></img>
                </div>
                <div class="col-lg-12 style">
                    <h3>DA APP</h3>     
                </div>
                <div class="col-lg-12 style">
                    <p>Purtroppo dalla App di Facebook non è possibile copiare direttamente il link come da PC.
                    </br>
                        Per ovviare, puoi taggare la nostra pagina sotto il contenuto razzista.
                        </br>
                        Per farlo digita <span class="tag-social">@segnala il razzista</span>
                    </p>
                </div>
                
            
                

        </div>


        <div class="container hide" id="come-segnalare-twitter">
            <div class="col-md-12 style">
                <h2>Come segnalarci contenuti razzisti su Twitter</h2>
            </div>
                <div class="col-lg-12 style">
                    <h3>DA PC E APP</h3>     
                </div>
                <div class="col-lg-12 style">
                    <p>Sia per PC che per APP la procedura è simile come mostrano le immagini.
                            </br>
                        Una volta copiato il link puoi inviarcelo a <a href="/segnala-razzista" title="segnala-link">questo link</a>.
                            </br>
                        In alternativa puoi usare l'hashtag <span class="tag-social">#segnalailrazzista</span>,
                            </br>
                         ma se ci invierai direttamente il link ci faciliterai il lavoro! :-)
                    </p>
                </div>
                <div class="col-lg-12 style">
                    <img class="img-responsive" src="{{ asset('img/site/come_segnalare_twitter.png') }}"></img>
                </div>
                <div class="col-lg-12 style">
                    <img class="img-responsive" src="{{ asset('img/site/come_segnalare_twitter2.png') }}"></img>
                </div>
                

        </div>

<script type="text/javascript">


$( document ).ready(function() {
    var istruzioni_fb = $("#come-segnalare-facebook");
    var istruzioni_tw = $("#come-segnalare-twitter");

    $( 'a#comesegnalafacebook' ).click(function(e) {
        e.preventDefault();
       

        if (istruzioni_fb.hasClass("hide")) {
            istruzioni_fb.removeClass("hide");
            $("html, body").animate({
                scrollTop: $('#comesegnalafacebook').offset().top
            });
        }else{
            istruzioni_fb.addClass("hide");

        }
        
    });
    $( "a#comesegnalatwitter" ).click(function(e) {
        
        e.preventDefault();

        if (istruzioni_tw.hasClass("hide")) {
            istruzioni_tw.removeClass("hide");
            $("html, body").animate({
                scrollTop: $('#comesegnalatwitter').offset().top
            });
        }else{
            istruzioni_tw.addClass("hide");

        }
        
    });
});

</script>
       


<!-- <div class="card-columns">

     <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body">
         
          <div class="fb-comment-embed" data-href="https://www.facebook.com/GeoPoliticalCenter/posts/1732128990206148?comment_id=1732161633536217&reply_comment_id=1732251150193932&comment_tracking=%7B%22tn%22%3A%22R7%22%7D" data-width="auto" data-include-parent="false"></div>
          
        </blockquote>
      </div>

       <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body">
          
          <div class="fb-comment-embed" data-href="https://www.facebook.com/zuck/posts/10102577175875681?comment_id=1193531464007751" data-width="auto" data-include-parent="false"></div>
          
        </blockquote>
      </div>
       <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body">
          <div class="fb-comment-embed" data-href="https://www.facebook.com/GeoPoliticalCenter/posts/1732128990206148?comment_id=1732161633536217&reply_comment_id=1732251150193932&comment_tracking=%7B%22tn%22%3A%22R7%22%7D" data-width="auto" data-include-parent="false"></div>
          
          
        </blockquote>
      </div>
      <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body">
          <div class="fb-comment-embed" data-href="https://www.facebook.com/GeoPoliticalCenter/posts/1732128990206148?comment_id=1732161633536217&reply_comment_id=1732251150193932&comment_tracking=%7B%22tn%22%3A%22R7%22%7D" data-width="auto" data-include-parent="false"></div>
          
          
        </blockquote>
      </div>
      <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body">
          <div class="fb-comment-embed" data-href="https://www.facebook.com/GeoPoliticalCenter/posts/1732128990206148?comment_id=1732161633536217&reply_comment_id=1732251150193932&comment_tracking=%7B%22tn%22%3A%22R7%22%7D" data-width="auto" data-include-parent="false"></div>
          
          
        </blockquote>
      </div>
      <div class="card p-3">
        <blockquote class="blockquote mb-0 card-body twitter-tweet" data-conversation="none" data-lang="en"><p lang="in" dir="ltr">si...final ury-serb 🍻</p>&mdash; Андрей (@13_shiba) <a href="https://twitter.com/13_shiba/status/1007182818802388992?ref_src=twsrc%5Etfw">June 14, 2018</a>
        </blockquote>

      </div>


</div> -->






@endsection