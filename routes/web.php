<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/come-segnalare', function () {
    return view('come-segnalare');
});


Route::resource('/posts/', 'PostController');


//Route::resource('posts','PostController')->middleware('admin');
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/admin/', 'AdminController')->middleware('admin');

//Route::get('admin/routes', 'HomeController@admin')->middleware('admin');

Auth::routes();

